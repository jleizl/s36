
const Task = require('../models/Task.js')


module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	
	return newTask.save().then((savedTask, error) => {
		
		if(error) {
			console.log(error)
			return false
		} else {
			return savedTask 
		}
		
	})
}	


module.exports.getTask = (taskId) => {

return Task.findById(taskId).then(result => {
	return result
	})
}


module.exports.completeTask = (taskId, newContent) => {
	
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		}
		result.name = newContent.name,
		result.status = newContent.status
		
		return result.save().then((completedTask, error) => {
			if(error) {
				console.log(error)
				return false
			} else {
				return completedTask
			}
			
		})
	})
}

