

const express = require('express')
const mongoose = require('mongoose')


const taskRoutes = require('./routes/taskRoutes.js')


const app = express()
const port = 3001



app.use(express.json())
app.use(express.urlencoded({extended: true}))




mongoose.connect(`mongodb://jleizl93:admin123@ac-floig6u-shard-00-00.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-01.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-02.2zb8dt2.mongodb.net:27017/s36?ssl=true&replicaSet=atlas-12lysg-shard-0&authSource=admin&retryWrites=true&w=majority`, 
    {
    useNewUrlParser: true,
    useUnifiedTopology: true
    }
)

let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.once('open', () => console.log('Connected to MongoDB!'))


// Routes
app.use('/tasks', taskRoutes)



app.listen(port, () => console.log(`Server is running at port: ${port}`))
