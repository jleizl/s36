
const express = require('express')


const TaskController = require('../controllers/TaskController.js')


const router = express.Router()



router.post('/create', (req, res) =>{
	
	TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})


router.get('/:id', (req, res) => {
	TaskController.getTask(req.params.id).then((getTask) => res.send(getTask))
})

router.put('/:id/complete', (req, res) => {
	
	TaskController.completeTask(req.params.id, req.body).then((completeTask) => res.send(completeTask))
})


module.exports = router
